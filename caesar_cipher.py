#!/usr/bin/env python

story = "Xoqy Tzcfsm wg o amhvwqoz qvofoqhsf qfsohsr cb hvs gdif ct o acasbh hc vszd qcjsf ob wbgittwqwsbhzm dzobbsr voqy. Vs vog pssb fsuwghsfsr tcf qzoggsg oh AWH hkwqs pstcfs, pih vog fsdcfhsrzm bsjsf doggsr oqzogg. Wh vog pssb hvs hforwhwcb ct hvs fsgwrsbhg ct Sogh Qoadig hc psqcas Xoqy Tzcfsm tcf o tsk bwuvhg soqv msof hc sriqohs wbqcawbu ghirsbhg wb hvs komg, asobg, obr shvwqg ct voqywbu."
shift = 12

# def ceasar(string, shift_rounds)
# -26 < shift_rounds < 26
# if you use round i to encode, then use -i to decode
ceasar = lambda s,n:''.join([(lambda c,u:c.upper() if u else c)(("abcdefghijklmnopqrstuvwxyz"*2)[ord(c.lower())-97+n%26],c.isupper())if c.isalpha() else c for c in s])

print(ceasar(story, shift))